﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;

// Used for writing to a file
using System.IO;
// Used to serialize an object to binary format
using System.Runtime.Serialization.Formatters.Binary;
// Used to serialize into XML
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.Xml;
using System.Runtime.Serialization;

namespace WpfExamples
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        ///   Constructor.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        #region Paint

        /// <summary>
        ///   On select button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectBtn_Click(object sender, RoutedEventArgs e)
        {
            (this.Resources["myInkCanvasEditingModes"] as MyInkCanvasEditingModes).Mode = InkCanvasEditingMode.Select;
        }

        /// <summary>
        ///   On draw button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DrawBtn_Click(object sender, RoutedEventArgs e)
        {
            (this.Resources["myInkCanvasEditingModes"] as MyInkCanvasEditingModes).Mode = InkCanvasEditingMode.Ink;
        }

        /// <summary>
        ///   On erase button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EraseBtn_Click(object sender, RoutedEventArgs e)
        {
            (this.Resources["myInkCanvasEditingModes"] as MyInkCanvasEditingModes).Mode = InkCanvasEditingMode.EraseByPoint;
        }

        #endregion

        #region Survey

        /// <summary>
        ///   Toggle survey.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toggleSurvey(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #region LoadFile

        private async void LoadFileBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Choose text file *.txt";
            fileDialog.Filter = "All supported text files|*.txt";
            if (fileDialog.ShowDialog() == true)
            {
                try
                {
                    using (StreamReader sr = new StreamReader(fileDialog.FileName))
                    {
                        string line = await sr.ReadToEndAsync();
                        LoadedFileText.Text = line;
                    }
                }
                catch (FileNotFoundException ex)
                {
                    LoadedFileText.Text = ex.Message;
                }
            }
            else
            {
                LoadedFileText.Text = "";
            }
        }

        #endregion

        #region LoadAndSaveToFile

        private void LoadFileBtn2_Click(object sender, RoutedEventArgs e)
        {
            Animal bowser = new Animal("Bowser", 45, 25);

            // Serialize the object data to a file
            Stream stream = File.Open("AnimalData.dat", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();

            // Send the object data to the file
            bf.Serialize(stream, bowser);
            stream.Close();

            // Delete the bowser data
            bowser = null;

            // Read object data from the file
            stream = File.Open("AnimalData.dat", FileMode.Open);
            bf = new BinaryFormatter();

            bowser = (Animal)bf.Deserialize(stream);
            stream.Close();

            Console.WriteLine(bowser.ToString());

            // Change bowser to show changes were made
            bowser.Weight = 50;

            string pathToDesktop1 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            pathToDesktop1 += @"\bowser.xml";

            // XmlSerializer writes object data as XML
            XmlSerializer serializer = new XmlSerializer(typeof(Animal));
            using (TextWriter tw = new StreamWriter(pathToDesktop1))
            {
                serializer.Serialize(tw, bowser);
            }

            // Delete bowser data
            bowser = null;

            // Deserialize from XML to the object
            XmlSerializer deserializer = new XmlSerializer(typeof(Animal));
            TextReader reader = new StreamReader(pathToDesktop1);

            object obj = deserializer.Deserialize(reader);
            bowser = (Animal)obj;
            reader.Close();

            Console.WriteLine(bowser.ToString());

            // Save a collection of Animals
            List<Animal> theAnimals = new List<Animal>
            {
                new Animal("Mario", 60, 30),
                new Animal("Luigi", 55, 24),
                new Animal("Peach", 40, 20)
            };

            string pathToDesktop2 = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            pathToDesktop2 += @"\animals.xml";

            using (Stream fs = new FileStream(pathToDesktop2,
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                XmlSerializer serializer2 = new XmlSerializer(typeof(List<Animal>));
                serializer2.Serialize(fs, theAnimals);
            }

            // Delete list data
            theAnimals = null;

            // Read data from XML
            XmlSerializer serializer3 = new XmlSerializer(typeof(List<Animal>));

            using (FileStream fs2 = File.OpenRead(pathToDesktop2))
            {
                theAnimals = (List<Animal>)serializer3.Deserialize(fs2);
            }


            foreach (Animal a in theAnimals)
            {
                Console.WriteLine(a.ToString());
            }

            Console.ReadLine();

            LoadedFileText2.Text = "Files succesfully serialized to desktop";
        }

        public static Task<T> DeserializeObjectAsync<T>(string xml)
        {
            using (StringReader reader = new StringReader(xml))
            {
                using (XmlReader xmlReader = XmlReader.Create(reader))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                    T theObject = (T)serializer.ReadObject(xmlReader);
                    return Task.FromResult(theObject);
                }
            }
        }

        #endregion
    }
}
