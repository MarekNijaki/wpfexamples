﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfExamples
{
    /// <summary>
    ///   Logika interakcji dla klasy ImageCtrl.xaml
    /// </summary>
    public partial class ImageCtrl : UserControl
    {
        /// <summary>
        ///   Custom width property.
        /// </summary>
        private double _customWidthProp;

        /// <summary>
        ///   Custom width property.
        /// </summary>
        public double CustomWidthProp
        {
            get
            {
                return _customWidthProp;
            }

            set
            {
                _customWidthProp = value;
                this.Width = _customWidthProp;
            }
        }

        /// <summary>
        ///   Custom depencency propery.
        /// </summary>
        public List<ImageData> ImagesDataSource
        {
            get
            {
                return (List<ImageData>)GetValue(ImagesDataSourceProperty);
            }
            set
            {
                SetValue(ImagesDataSourceProperty, value);
            }
        }

        /// <summary>
        ///   Custom depencency propery handling.
        /// </summary>
        public static readonly DependencyProperty ImagesDataSourceProperty =
            DependencyProperty.Register("ImagesDataSource", typeof(List<ImageData>), typeof(ImageCtrl),
            new FrameworkPropertyMetadata(null, new PropertyChangedCallback((s, e) =>
            {
                var source = s as ImageCtrl;
                source.itemsControl.ItemsSource = (List<ImageData>)e.NewValue;
            })));

        /// <summary>
        ///   Constructor.
        /// </summary>
        public ImageCtrl()
        {
            InitializeComponent();

            List<ImageData> imagesData = new List<ImageData>();
            imagesData.Add(new ImageData("", null));
            this.DataContext = imagesData;
        }

        /// <summary>
        ///   On load image button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadImageBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Choose picture";
            fileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|Portable Network Graphic (*.png)|*.png";
            if(fileDialog.ShowDialog() == true)
            {
                BitmapImage bitmapImage = new BitmapImage(new Uri(fileDialog.FileName));

                List<ImageData> imagesData = new List<ImageData>();
                imagesData.Add(new ImageData(fileDialog.FileName, bitmapImage));
                this.DataContext = imagesData;
            }
            else
            {
                this.DataContext = null;
            }
        }
    }
}
