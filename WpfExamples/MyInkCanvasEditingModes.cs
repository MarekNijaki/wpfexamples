﻿using System.ComponentModel;
using System.Windows.Controls;

namespace WpfExamples
{
    /// <summary>
    ///   InkCanvas editing modes.
    /// </summary>
    class MyInkCanvasEditingModes : INotifyPropertyChanged
    {
        /// <summary>
        ///   Interface implementation.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///   InkCanvas editing mode.
        /// </summary>
        private InkCanvasEditingMode _mode;

        /// <summary>
        ///   InkCanvas editing mode.
        /// </summary>
        public InkCanvasEditingMode Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Mode"));
                }
            }
        }

        /// <summary>
        ///   Constructor.
        /// </summary>
        public MyInkCanvasEditingModes()
        {
            Mode = InkCanvasEditingMode.Select;
        }
    }
}
