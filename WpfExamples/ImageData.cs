﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WpfExamples
{
    /// <summary>
    ///   Image data.
    /// </summary>
    public class ImageData
    {
        public string PathToImage { get; set; }
        public string NameOfImage { get; set; }
        public BitmapImage ImageTexture { get; set; }
        public bool IsFound { get; set; }

        /// <summary>
        ///   Constructor.
        /// </summary>
        public ImageData()
        {
            this.PathToImage = "no_image.png";
            this.NameOfImage = "no_image";
        }

        /// <summary>
        ///   Constructor.
        /// </summary>
        /// <param name="pathToImage"></param>
        /// <param name="imageTexture"></param>
        public ImageData(string pathToImage, BitmapImage imageTexture)
        {
            string absolutePath = Path.Combine(Directory.GetCurrentDirectory(), pathToImage);
           
            this.PathToImage = pathToImage;
            this.NameOfImage = Path.GetFileName(pathToImage);
            this.ImageTexture = imageTexture;

            IsFound = File.Exists(absolutePath) && (imageTexture!= null);
        }
    }
}
